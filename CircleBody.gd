extends RigidBody2D

var ball_color: Color setget , get_ball_color
var size: Vector2 setget set_scale, get_scale
var body_entered = []

func set_scale(value: Vector2):
	$CollisionShape2D.scale = value
	material.set_shader_param("scale", value)
	if $CollisionShape2D.scale.x < 0.1:
		queue_free()

func get_scale() -> Vector2:
	return $CollisionShape2D.scale

func get_ball_color() -> Color:
	var color: Vector3 = material.get_shader_param("color")
	return Color(color.x, color.y, color.z)

func _ready() -> void:
	set_scale(scale)

func _physics_process(delta: float):
	for other in body_entered:
		if is_instance_valid(other):
			if self.size.x < other.size.x or (self.size == other.size and self.position.x < other.position.x):
				get_parent().get_parent().Collision(self, other, delta)

func _on_CircleBody_body_entered(body: Node) -> void:
	if body.is_in_group("Circle"):
		body_entered.append(body)

func _on_CircleBody_body_exited(body: Node) -> void:
	if body.is_in_group("Circle"):
		body_entered.erase(body)
