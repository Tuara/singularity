# [⚫ Singularity ⚪][game page]

> Fill the square holes with the round[ ][​]pegs

[![banner](public/banner.png)][play]

[▶️ P L A Y][play] | [❤️ LIKE][game page]

A physics-based puzzle game made for the [Global Game Jam] 2022.

- Theme: **"Duality"**
- [Diversifiers]:
  - Runtime Fun Time
    > Create a game without using any external assets, all your content must be generated from code at runtime.
  - Stick Together
    > Make a game that revolves around grouping characters or objects.
- Team:
  - [Thomas Ganthier] (Scripting, Level design)
  - [Thibault Lemaire] (Scripting, Shaders)


[game page]: https://globalgamejam.org/2022/games/singularity-9-0
[play]: https://tuara.gitlab.io/singularity
[Global Game Jam]: https://globalgamejam.org/
[Diversifiers]: https://globalgamejam.org/news/ggj-2022-diversifiers
[Thomas Ganthier]: https://gitlab.com/Tuara
[Thibault Lemaire]: https://gitlab.com/ThibaultLemaire

## Opening the source with [Godot]

> 🛈 If you're used to Godot, nothing special here.

This game is made with the [Godot] open-source engine (v3.3.3.stable). Here are the steps to open and edit it:

1. [Download and Install Godot][Download Godot]
2. [Download the source] (or clone it with [Git])
3. Start Godot
4. Go to `Import`
5. Select the `.zip` file you downloaded in 2.
6. Choose an installation folder (create it if needed)
7. Click `Import & Edit`

[Godot]: https://godotengine.org/
[Download Godot]: https://godotengine.org/download
[Download the source]: https://gitlab.com/Tuara/singularity/-/archive/master/singularity-master.zip
[Git]: https://git-scm.com/

## About the diversifiers

### Runtime Fun Time

Technically godot always uses an external `.pck` file to load assets and scripts, so whether or not we qualify is up for debate.

Regardless, we have chosen to interpret that diversifier as "No raw images or sounds".

For sound we made the easy choice to not include any, so you're free to listen to your most relaxing music while playing (we suggest [Wintergatan - Marble Machine][marblemachine]).

And for textures, we went the shader way.
There is not a single image file in the project, everything you see on your screen is Godot + shaders.

[marblemachine]: https://www.youtube.com/watch?v=IvUU8joBb1Q

### Stick Together

Although not as central as we would have liked, this diversifier is best seen in level 5.
Where the two bubbles, individually, cannot pass the static barrier without losing too much matter, merging them into a single big one lets you finish the level.

## License

This game and all its code is released under [CC-BY-SA-4.0].

Original [black hole shader] by Marc Gilleron

Absorbtion area texture repurposed from the [Noise chapter] of The Book of Shaders.

[Noise chapter]: https://thebookofshaders.com/11/
[black hole shader]: https://github.com/Zylann/godot_blackhole_plugin/blob/master/addons/zylann.blackhole/blackhole_shader.tres
[CC-BY-SA-4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[​]: https://scp-wiki.wikidot.com/scp-2998
