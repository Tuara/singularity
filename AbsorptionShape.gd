extends Polygon2D

var body_entered = []
const rate: float = 0.5

func _ready() -> void:
	$Area2D/CollisionPolygon2D.polygon = self.polygon

func _physics_process(delta: float) -> void:
	if not body_entered.empty():
		var absorbed: float = rate * delta
		for body in body_entered:
			body.size -= Vector2(absorbed, absorbed)
			get_parent().get_parent().RemoveQuantity(str(body.ball_color), absorbed) # Réduire la quantité d'appels + Envoyer signal

func _on_Area2D_body_entered(body: Node) -> void:
	if body.is_in_group("Circle"):
		body_entered.append(body)

func _on_Area2D_body_exited(body: Node) -> void:
	if body.is_in_group("Circle"):
		body_entered.erase(body)
