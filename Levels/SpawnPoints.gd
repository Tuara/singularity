extends Node

onready var ball_scene = preload("res://CircleBody.tscn")
var spawnPoints = []
var colors = [Vector3(1,1,1), Vector3(0,0,0)]

func _ready() -> void:
	for child in get_children():
		if child is Position2D:
			spawnPoints.append(child)
	randomize()

func _on_Timer_timeout() -> void:
	var randPos = randi() % spawnPoints.size()
	var randColor = randi() % colors.size()
	var ball_instance = ball_scene.instance()
	ball_instance.position = spawnPoints[randPos].position
	ball_instance.material.set_shader_param("color", colors[randColor])
	ball_instance.size = Vector2(1, 1)
	get_parent().get_node("Circles").add_child(ball_instance)
